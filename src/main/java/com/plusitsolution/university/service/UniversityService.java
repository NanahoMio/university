package com.plusitsolution.university.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.plusitsolution.university.domain.SubjectDomain;
import com.plusitsolution.university.domain.UniversityDomain;

@Service
public class UniversityService {
	
	public Map<Integer, UniversityDomain> UNIVERSITY_MAP = new HashMap<>();
//	public Map<Integer, SubjectDomain> SUBJECT_MAP = new HashMap<>();
	
	
//	@PostConstruct
//	private void allSubject() {
//		SubjectDomain subject1 = new SubjectDomain(1, "Java & Spring Boots" ,"Lang");
//		SubjectDomain subject2 = new SubjectDomain(2, "Object Oriented Programing" ,"Egg");
//		SUBJECT_MAP.put(1, subject1);
//		SUBJECT_MAP.put(2, subject2);
//	}
	
	@PostConstruct
	private void allStudent() {
		UniversityDomain student1 = new UniversityDomain(1, "Patiparn", "Seedanoi", 4);
		UniversityDomain student2 = new UniversityDomain(2, "Nong", "Dee", 4);
		UniversityDomain student3 = new UniversityDomain(4, "Nong", "Pong", 4);
		UNIVERSITY_MAP.put(1, student1);
		UNIVERSITY_MAP.put(2, student2);
		UNIVERSITY_MAP.put(4, student3);
	}
	
	private static AtomicInteger counter = new AtomicInteger();
	
	public void resgisterStudent(String firstname, String lastname, Integer year) {
		Integer id  = counter.incrementAndGet();
		for(Map.Entry<Integer, UniversityDomain> entry : UNIVERSITY_MAP.entrySet()) {
//			UniversityDomain studentDetail =  entry.getValue();
			Integer key = entry.getKey();
		    if(id == key) {
		    	id = id+1;
		    }
		}
		UniversityDomain domain = new UniversityDomain(id, firstname, lastname, year);
		UNIVERSITY_MAP.put(id, domain);
			
	}
	
	public UniversityDomain getStudentInfo(Integer idStudent) {
		UniversityDomain domain = UNIVERSITY_MAP.get(idStudent);
		if(domain == null) {
			throw new IllegalArgumentException("Student not found");
		}
		return domain;
	}
	
	public void editStudentInfo(Integer idStudent, String firstname, String lastname, Integer year) {
		UniversityDomain domain = new UniversityDomain(idStudent, firstname, lastname, year);
		for(Map.Entry<Integer, UniversityDomain> entry : UNIVERSITY_MAP.entrySet()) {
//			UniversityDomain studentDetail =  entry.getValue();
			Integer key = entry.getKey();
		    if(idStudent == key) {
		    	UNIVERSITY_MAP.put(idStudent, domain);
		    	break;
		    }
		    else if(idStudent != key) {
		    	throw new IllegalArgumentException("Student not found");
		    }
		}
	}
	
	public void deleteStudentInfo(Integer idStudent) {
//		UniversityDomain domain = new UniversityDomain(idStudent);
		for(Map.Entry<Integer, UniversityDomain> entry : UNIVERSITY_MAP.entrySet()) {
			UniversityDomain studentDetail =  entry.getValue();
			Integer key = entry.getKey();
		    if(idStudent == key) {
		    	UNIVERSITY_MAP.remove(idStudent);
		    	break;
		    }
		    else if(idStudent != key) {
		    	throw new IllegalArgumentException("Student not found");
		    }
		}
	}
	
	public List<UniversityDomain> getAllStudent(){
		List<UniversityDomain> list = new ArrayList<>();
		UNIVERSITY_MAP.entrySet().stream().forEach( domain -> {
			list.add(domain.getValue());
		});
		return list;
	}
	
//	public  SubjectDomain getSubjectInfo(Integer idSubject) {
//		SubjectDomain domain = SUBJECT_MAP.get(idSubject);
//		if(domain == null) {
//			throw new IllegalArgumentException("Subject not found");
//		}
//		return domain;
//		
//	}
	
}
