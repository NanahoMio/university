package com.plusitsolution.university.domain;

public class UniversityDomain {
	//Student
	private Integer idStudent;
	private String firstname;
	private String lastname;
	private Integer year;
	
	public UniversityDomain() {
		
	}
	
	public UniversityDomain(Integer idStudent, String firstname, String lastname, Integer year) {
		this.idStudent = idStudent;
		this.firstname = firstname;
		this.lastname = lastname;
		this.year = year;
	}
	
	public UniversityDomain(Integer idStudent) {
		this.idStudent = idStudent;
	}

	public Integer getIdStudent() {
		return idStudent;
	}

	public void setIdStudent(Integer idStudent) {
		this.idStudent = idStudent;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	
}
