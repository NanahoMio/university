package com.plusitsolution.university.domain;

public class SubjectDomain {
	//Subject
	private Integer idSubject;
	private String nameSubject;
	private String nameTeacher;
	
	public SubjectDomain(Integer idSubject, String nameSubject, String nameTeacher) {
		this.idSubject = idSubject;
		this.nameSubject = nameSubject;
		this.nameTeacher = nameTeacher;
	}
	
	public Integer getIdSubject() {
		return idSubject;
	}
	public void setIdSubject(Integer idSubject) {
		this.idSubject = idSubject;
	}
	public String getNameSubject() {
		return nameSubject;
	}
	public void setNameSubject(String nameSubject) {
		this.nameSubject = nameSubject;
	}
	public String getNameTeacher() {
		return nameTeacher;
	}
	public void setNameTeacher(String nameTeacher) {
		this.nameTeacher = nameTeacher;
	}
	
}