package com.plusitsolution.university.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.plusitsolution.university.domain.SubjectDomain;
import com.plusitsolution.university.domain.UniversityDomain;
import com.plusitsolution.university.service.UniversityService;

@RestController
public class UniversityController {
	
	@Autowired
	private UniversityService service;
	
	@PostMapping("/resgisterStudent")
	public void resgisterStudent(@RequestParam("firstname") String firstname,@RequestParam("lastname") String lastname,@RequestParam("year") Integer year) {
		service.resgisterStudent(firstname, lastname, year);
	}
	
	@GetMapping("/studentInfo")
	public UniversityDomain getStudentInfo(@RequestParam("idStudent") Integer idStudent) {
		return service.getStudentInfo(idStudent);
	}
	
	@PutMapping("/editStudentInfo")
	public void editStudentInfo(@RequestParam("idStudent") Integer idStudent, @RequestParam("firstname") String firstname,@RequestParam("lastname") String lastname,@RequestParam("year") Integer year) {
		service.editStudentInfo(idStudent, firstname, lastname, year);
	}
	
	@DeleteMapping("/deleteStudentInfo")
	public void deleteStudentInfo(@RequestParam("idStudent") Integer idStudent) {
		service.deleteStudentInfo(idStudent);
	}
	
	@GetMapping("/allStudent")
	public List<UniversityDomain> getMovieList(){
		return service.getAllStudent();
	}
	
//	@GetMapping("/subjectInfo")
//	public SubjectDomain getSubjectInfo(@RequestParam("idSubject") Integer idSubject) {
//		return service.getSubjectInfo(idSubject);
//	}
	
	
}
